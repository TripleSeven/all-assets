--文件说明:本文件提供LoginUITask的热更模块的lua对象
--作者:baizihan
local function CreateLuaObject(csObj, luaObjHelper)
    -- 对象中的成员
    local luaHotfixObj = {}

	-- 构造函数
	function luaHotfixObj:ctor(name)
        Debug.Log("LoginUITask.txt.luaHotfixObj:LoginUITask "..name)
        --SLua.BJLuaObjHelper.IsSkipLuaHotfix = true;
        --csObj:LoginUIController_OnSelectServerClick()
        -- 跳转到LuaMainUI
        -- UIManager.Instance:StartUITask(UIIntent("LuaMainUITask"), false, false, nil, nil)
    end

	function luaHotfixObj:InitializeGlobals()
		Debug.Log("LoginUITask.txt.luaHotfixObj:InitializeGlobals enter s_isGlobalInitialized="..tostring(LoginUITask.s_isGlobalInitialized))
		if (LoginUITask.s_isGlobalInitialized)
		then
			return; 
		end
		
		Screen.sleepTimeout = SleepTimeout.NeverSleep;            
        SystemUtility.SetTimeScale(1);
        Time.maximumDeltaTime = 0.1;
        Debug.Log("LoginUITask.txt.luaHotfixObj:InitializeGlobals Quality "..QualitySettings.names[QualitySettings.GetQualityLevel() + 1]);
		
		local configDataLoader = GameManager.Instance.ConfigDataLoader;
        local ret = configDataLoader:UtilityInitialize();
        if (ret < 0) then
            local err = "Failed to initialize config, error: "..tostring(ret)..", "..configDataLoader.UtilityGetErrorCodeString(ret);
            Debug.LogError("配置加载错误："..err);
            self.m_loadConfigFailedMessage = "配置加载错误：" .. err;
        end


        -- 账号无关的本地储存设置
        LocalConfig.Instance = LocalConfig();
        LocalConfig.Instance:SetFileName(Application.persistentDataPath .. "/ProjectL_Config.txt");
        LocalConfig.Instance:Load();
        LocalConfig.Instance:Apply(true);

        -- 账号相关的本地储存设置
        LocalAccountConfig.Instance = LocalAccountConfig();

        -- 战斗过程的本地储存
        LocalProcessingBattle.Instance = LocalProcessingBattle();
        LocalProcessingBattle.Instance:SetFileName(Application.persistentDataPath .. "/ProjectL_Battle.txt");
        LocalProcessingBattle.ArenaInstance = LocalProcessingBattle();
        LocalProcessingBattle.ArenaInstance:SetFileName(Application.persistentDataPath .. "/ProjectL_ArenaBattle.txt");

        -- 注册NetWorkTransactionTask相关的全局事件
        NetWorkTransactionTask.EventReLoginBySession = {"+=", LoginUITask.NetWorkTransactionTask_OnReLoginBySession};
        NetWorkTransactionTask.EventReturnToLoginUI = {"+=", LoginUITask.NetWorkTransactionTask_ReturnToLoginUI};
        NetWorkTransactionTask.EventShowUIWaiting = {"+=", LoginUITask.NetWorkTransactionTask_OnEventShowUIWaiting};

        -- 注册UIManager相关的全局事件
        UIManager.Instance.EventReturnToLoginUI = {"+=", LoginUITask.NetWorkTransactionTask_ReturnToLoginUI};


        if (UIManager.Instance ~= null) then
            UIManager.Instance.EventAndroidBackKeyUp = {"+=", CommonUIController.OnAndroidBackKeyUp};
		end
		
        LoginUITask.m_serverlist:Clear();
        LoginUITask.m_curSelectServerID = -1;

        LoginUITask.s_isGlobalInitialized = true;

        UserGuideUITask.Initialize();

		Debug.Log("LoginUITask.txt.luaHotfixObj:InitializeGlobals return")
	end

		function luaHotfixObj:IsNeedLoadDynamicRes()
			Debug.Log("LoginUITask.txt.luaHotfixObj:IsNeedLoadDynamicRes enter")
            self.m_assets:Clear();

            if (self.m_curLoadingLayers.Count == 0) then
                AssetUtility.AddAssetToList(UIAsset.TouchHitFx, self.m_assets);
                AssetUtility.AddAssetToList(UIAsset.TouchLoopFx, self.m_assets);
                AssetUtility.AddSpriteAssetToList(UIAsset.LoginNoticeLabel, self.m_assets);
                AssetUtility.AddSpriteAssetToList(UIAsset.LoginActivityLabel, self.m_assets);
            else
				local hasServerListUIController = false;
                -- if (self.m_curLoadingLayers.Find({function(layer) return layer.m_layerName == "ServerListUIController"; end}) != null) then
				for i=self.m_curLoadingLayers.Count-1,0,-1 do
					if (self.m_curLoadingLayers[i].m_layerName == "ServerListUIController") then
						hasServerListUIController = true;
						break;
					end
				end
				if hasServerListUIController then
                    for i=1,#UIAsset.ServerStateSpritePath do
                        AssetUtility.AddSpriteAssetToList(UIAsset.ServerStateSpritePath[i], self.m_assets);
					end
                    local configDataLoader = GameManager.Instance.ConfigDataLoader;
					local allCharImage = configDataLoader:LuaGetAllConfigDataCharImageInfo();
					for image in Slua.iter(allCharImage) do
                        AssetUtility.AddSpriteAssetToList(image.SmallHeadImage, self.m_assets);
					end
                end
            end
			
			Debug.Log("LoginUITask.txt.luaHotfixObj:IsNeedLoadDynamicRes return "..tostring(self.m_assets.Count));

            return self.m_assets.Count > 0;
        end

		function luaHotfixObj:get_ShouldUsePDSDK()
			local si = LoginUITask.GetCurrentSelectServerInfo();
			Debug.Log("LoginUITask.txt.luaHotfixObj:get_ShouldUsePDSDK return "..tostring(si == nil or (not String.IsNullOrEmpty(si.m_loginAgentUrl))));
            return si == nil or (not String.IsNullOrEmpty(si.m_loginAgentUrl));
		end

		function luaHotfixObj:StartCoroutineKeepUpdatingServerList()
			Debug.Log("LoginUITask.txt.luaHotfixObj:StartCoroutineKeepUpdatingServerList enter ");
			local c = coroutine.create(function() self:KeepUpdatingServerList() end);
			coroutine.resume(c);
        end

        function luaHotfixObj:KeepUpdatingServerList()
			local ConfigableConstId_UpdateServerListIntervalSeconds = 245;
			Debug.Log("LoginUITask.txt.luaHotfixObj:KeepUpdatingServerList enter ");
            while(true) do
				Yield(WaitForSeconds( self.m_configDataLoader:UtilityGetConfigableConst(
                        ConfigableConstId_UpdateServerListIntervalSeconds)));
				if (self.State==3) then
					break;
				end
				Debug.Log("LoginUITask.txt.luaHotfixObj:KeepUpdatingServerList call DownloadServerListFile");
                self:DownloadServerListFile();
            end
			Debug.Log("LoginUITask.txt.luaHotfixObj:KeepUpdatingServerList exit ");
        end

		function luaHotfixObj:InitAllUIControllers()
			Debug.Log("LoginUITask.txt.luaHotfixObj:InitAllUIControllers enter");
			self:__callBase_InitAllUIControllers()

			if (self.m_serverListUIController == nil) then
				if (self.m_uiCtrlArray.Length > 1) then
					self.m_serverListUIController = self.m_uiCtrlArray[2]
				end
				if (self.m_serverListUIController ~= nil) then
					self.m_serverListUIController.EventOnClosed = {"+=",function(selectedServerID) self:ServerListUIController_OnServerListClosed(selectedServerID) end}
					local si = LoginUITask.GetCurrentSelectServerInfo()
					if (si == nil) then
						self.m_serverListUIController:SetServerList(LoginUITask.m_serverlist, LoginUITask.GetRecentLoginServerList(),
																	"", m_exsitCharsInfo);
					else
						self.m_serverListUIController:SetServerList(LoginUITask.m_serverlist, LoginUITask.GetRecentLoginServerList(),
																	si.m_roleListURL, m_exsitCharsInfo);
					end
				end
			end
		
			if (self.m_createCharaterUIController == nil) then
				if (self.m_uiCtrlArray.Length>2) then
					self.m_createCharaterUIController = self.m_uiCtrlArray[3]
				end
				if (self.m_createCharaterUIController ~= nil) then
					self.m_createCharaterUIController.EventOnCreate = {"+=",function() self:CreateCharacterUIController_OnCreate() end}
					self.m_createCharaterUIController.EventOnAutoName = {"+=",function() self:CreateCharacterUIController_OnAutoName() end}
				else
					Debug.LogError("CreateCharacterUIController is null");
					return
				end
			end
		
			if (self.m_loginCommonUIController == nil) then
				if (self.m_uiCtrlArray.Length>3) then
					self.m_loginCommonUIController = self.m_uiCtrlArray[4]
				end
				if (self.m_loginCommonUIController ~= nil) then
					self.m_loginCommonUIController:FadeIn(0.5, UnityEngine.Color.black,nil)
				else
					Debug.LogError("LoginCommonUIController is null")
					return
				end
				self.m_loginCommonUIController:InitTouchFx();
			end
		
			if (self.m_loginUIController == nil) then
				if (self.m_uiCtrlArray.Length>0) then
					self.m_loginUIController = self.m_uiCtrlArray[1]
				end
				if (self.m_loginUIController ~= nil) then
					self.m_loginUIController.EventSelectServer = {"+=",function() self:LoginUIController_OnSelectServerClick() end}
					self.m_loginUIController.EventOnLogin = {"+=", function() self:LoginUIController_OnLogin() end}
					self.m_loginUIController.EventOnSaveServerConfig = {"+=", function() self:LoginUIController_OnSaveServerConfig() end}
					self.m_loginUIController.EventAccountTextChanged = {"+=", function(text) self:LoginUIController_OnAccountTextChanged(text) end}
					self.m_loginUIController.EventOpenUserCenter = {"+=",function() self:LoginUIController_OnOpenUserCenter() end}
					self.m_loginUIController.EventOnCloseAnnouncePanel = {"+=",function() self:LoginUIController_OnCloseAnnouncePanel() end}
					self.m_loginUIController.EventOnOpenAnnouncePanel = {"+=",function() self:LoginUIController_OnOpenAnnouncePanel() end}
					
					-- 紫龙SDK相关事件
					PDSDK.m_eventLoginFailed = {"+=", function() self:PDSK_OnLoginFailed() end};
					PDSDK.m_eventLoginSuccess = {"+=", function(s1, s2, s3, s4) self:PDSDK_OnLoginSuccess(s1, s2, s3, s4) end};
					PDSDK.m_eventLogoutSuccess = {"+=", function() self:PDSDK_OnLogoutSuccess() end};
					PDSDK.m_eventInitSuccess = {"+=", function() self:PDSDK_OnInitSucess() end};
					PDSDK.m_eventInitFailed = {"+=", function() self:PDSDK_OnInitFailed() end};
					local SoundTableId_LoginMusic = 5
					AudioUtility.PlaySound(self.m_configDataLoader:UtilityGetSound(SoundTableId_LoginMusic))
					UIUtility.SetLongFrame()
					if (LoginUITask.GetCurrentSelectServerInfo() ~= null and self.ShouldUsePDSDK and not LoginUITask.m_isAutoRelogin) then
						self:PDLogin()
					end
				
					self:SetDailyPushNotifications()
					self:StartCoroutineKeepUpdatingServerList()
				else
					Debug.LogError("InitAllUIControllers LoginUIController is null")
					return
				end
			end
		
			UIUtility.ActivateLayer(self.m_createCharaterUIController,false)
		
			if (not String.IsNullOrEmpty(self.m_loadConfigFailedMessage)) then
				self.m_loginUIController:ShowMessage(self.m_loadConfigFailedMessage, 60, true);
			end
		
			-- 第二层UI游戏没用到，关闭（Camera）以避免UI特效被重复渲染
			local groupRoot2 = GameObject.Find("UILayerGroupRoot2")
			if (groupRoot2 ~= null) then
				groupRoot2:SetActive(false)
			end
			Debug.Log("LoginUITask.txt.luaHotfixObj:InitAllUIControllers exit");
		end

    return luaHotfixObj
end

local LoginUITask = {
    -- cs层会调用这个方法，不可随意命名
    HotFixObject = function(csObj, luaObjHelper)
        -- 创建lua对象
        local luaObj = CreateLuaObject(csObj, luaObjHelper)

        -- 让CS代码感知luaobj存在
        luaObjHelper:SetLuaObj(luaObj)

        -- 让luaobj继承 CS 对象
        luaObj = Slua.HotFixObj(csObj, luaObj)
    end
}

return LoginUITask
