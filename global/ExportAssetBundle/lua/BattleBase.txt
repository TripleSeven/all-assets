
--文件说明:本文件提供BattleBase的热更模块的lua对象
local function CreateLuaObject(csObj, luaObjHelper)
    -- 对象中的成员
    local luaHotfixObj = {}

    function luaHotfixObj:IsActorsAllKillBySkillClass(heroIds, skillClass)
        --Debug.Log("BattleBase.IsActorsAllKillBySkillClass In Lua")
		
		if (heroIds.Count <= 0) then
            return false;
		end

        local matchCount = 0;

		for heroId in Slua.iter(heroIds) do        
            local a = self:GetActorByHeroId(heroId);
            if (a ~= nil) then
				if (a:IsDead()) then
					if (IsSkillClassMatch(a:GetLastDamageBySkill(), skillClass)) then
						matchCount = matchCount + 1;
					else
						return false;
					end            
				else            
					return false;
				end
			end
        end

        return matchCount > 0;
    end
	
	function IsSkillClassMatch(skillInfo, skillClass)		            
		if (skillInfo == nil) then   
			return false;
		end

        if (skillClass == 1) then        
            -- 普攻
            return skillInfo:IsNormalAttack();        
        elseif (skillClass == 2) then           
            -- 技能
            return not skillInfo:IsNormalAttack();            
        else                 
		   return false;
        end
	end

    return luaHotfixObj
end

local BattleBase = {
    -- cs层会调用这个方法，不可随意命名
    HotFixObject = function(csObj, luaObjHelper)
        -- 创建lua对象
        local luaObj = CreateLuaObject(csObj, luaObjHelper)

        -- 让CS代码感知luaobj存在
        luaObjHelper:SetLuaObj(luaObj)

        -- 让luaobj继承 CS 对象
        luaObj = Slua.HotFixObj(csObj, luaObj)
    end
}

return BattleBase
